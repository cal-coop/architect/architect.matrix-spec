(asdf:defsystem #:architect.matrix-spec
  :description "Dependency management for the matrix spec."
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :licence "Artistic License 2.0"
  :depends-on ("legit")
  :serial t
  :components ((:file "manage-spec")
               (:file "ensure-cloned")))
