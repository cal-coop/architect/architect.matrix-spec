#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.matrix-spec
  (:use #:cl)
  (:export
   #:update
   #:clone
   #:ensure-cloned
   #:spec
   #:*local-repo*
   #:*remote*))

(in-package #:architect.matrix-spec)

(defvar *local-repo*
  (asdf:system-relative-pathname '#:architect.matrix-spec "matrix-doc/"))

(defvar *remote* "https://github.com/matrix-org/matrix-doc")

(defun clone ()
  (legit:clone *remote* *local-repo*))

(defun update ()
  (let ((repository (make-instance 'legit:repository :location *local-repo*)))
    (legit:pull repository)))

(defun ensure-cloned ()
  (unless (uiop:directory-exists-p *local-repo*)
    (clone)))

(defun spec (path)
  (let ((new-path (uiop:subpathname *local-repo* path)))
    (and (or (uiop:file-exists-p new-path) (uiop:directory-exists-p new-path))
         new-path)))
